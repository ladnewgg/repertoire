import time
wait = .200
base = wait

def stop() :
	print("\n    Vous avez décidé d'annuler.")
	time.sleep(wait)
	print("\n    Retour au menu principal.")
	return

def cheh() :
	print("\n    So...")
	time.sleep(1)
	print("\n    You chose...")
	time.sleep(1)
	print("\n    Death")
	time.sleep(1)
	print("\n    Say \"goodbye\" to you contacts")
	while True :
		if input() == "goodbye" :
			with open("repertoire.csv", "w") as f :
				for i in range(15) :
					f.write("You got Fucked\n")
			print("\n    goodbye !")
			break
	return

def list_contact() :
	with open("repertoire.csv", "r") as f :
		fread, char, contact, liste = f.read(), [], [], []
	for c in fread :
		if c == " " :
			contact.append("".join(char))
			char = []
		elif c == "\n" :
			contact.append("".join(char))
			liste.append(contact)
			char, contact = [], []
		else :
			char.append(c)
	return liste

def search() :
	liste, search, results = list_contact(), input("\n    Entrez ce que vous souhaitez chercher (peut être un prénom, nom ou numéro de téléphone) : "), []
	time.sleep(wait)
	for i in range(len(liste)) :
		sous_liste = liste[i]
		for j in range(len(sous_liste)) :
			if search.upper() == sous_liste[j].upper() :
				results.append(liste[i])
	results = [results[i] for i in range(len(results)) if results[i] not in results[:i]]		#sert a supprimer les résultats en double (solution trouvée sur internet...)
	if len(results) == 0 :
		print("    Aucun contact n'a été trouvé...")
		time.sleep(wait)
	elif len(results) == 1 :
		sous_liste = results[0]
		print("\n    Le contact suivant a été trouvé :\n")
		time.sleep(wait)
		print(f"		Prénom : {sous_liste[0]}")
		time.sleep(wait)
		print(f"		  Nom  : {sous_liste[1]}")
		time.sleep(wait)
		print(f"		Numéro : {sous_liste[2]}")
		time.sleep(wait)
	elif len(results) > 1 :
		print(f"\n    Les {len(results)} contacts suivants ont étés trouvés :")
		time.sleep(wait)
		for i in results :
			print(f"\n		Prénom : {i[0]}")
			time.sleep(wait)
			print(f"		  Nom  : {i[1]}")
			time.sleep(wait)
			print(f"		Numéro : {i[2]}")
			time.sleep(wait)
	print("\n    Retour au menu principal.")
	return

def add() :
	print("\n    Veuillez entrer les informations du contact (ou \"stop\" pour annuler).\n")
	time.sleep(wait)
	Name = input("    Prénom : ")
	time.sleep(wait)
	if Name == "stop" :
		stop()
		return
	Surname = input("      Nom  : ")
	time.sleep(wait)
	if Surname == "stop" :
		stop()
		return
	Number = input("    Numéro : ")
	time.sleep(wait)
	if Number == "stop" :
		stop()
		return
	with open("repertoire.csv", "a") as f :
		f.write(f"{Name} {Surname} {Number}\n")
	print("\n    Le contact a bien été enregistré !")
	time.sleep(wait)
	print("\n    Retour au menu Principal.\n")
	time.sleep(wait)
	return

def show() :
	print(f"\n    Etes vous sur que vous voulez tout afficher ?")
	time.sleep(wait)
	print("    Cette liste peut etre tres longue...")
	time.sleep(wait)
	print(f"    (Vous avez {len(list_contact())} contacts)")
	time.sleep(wait)
	if "stop" == input("\n    Entrez \"stop\" pour arrêter ou n'importe quelle touche pour continuer : ") :
		time.sleep(wait)
		stop()
		return
	liste = list_contact()
	print("\n    Voici la liste de tout les contacts :")
	time.sleep(wait)
	for i in liste :
		print(f"\n		Prénom : {i[0]}")
		time.sleep(.050)
		print(f"		  Nom  : {i[1]}")
		time.sleep(.050)
		print(f"		Numéro : {i[2]}")
		time.sleep(.050)
	print("\n    Retour au menu principal.")
	return

def edit() :
	print("\n    Savez vous quel contact vous souhaitez supprimer ou souhaitez vous les afficher ?\n")
	time.sleep(wait)
	print("	1 - Afficher les contacts")
	time.sleep(wait)
	print("	2 - Modifier un contact")
	time.sleep(wait)
	if 1 == int(input("\n    Entrez un nombre ici : ")) :
		time.sleep(wait)
		liste = list_contact()
		print(f"\n    Vous avez {len(liste)} contacts :")
		time.sleep(wait)
		count = 0
		for i in liste :
			count += 1
			print(f"\n    {count} - {i[0]}   {i[1]}   {i[2]}")
			time.sleep(.050)
	while True :
		edit = input("\n    Entrez le numéro du contact que vous souhaitez modifier, ou stop pour annuler : ")
		time.sleep(wait)
		try :
			edit = int(edit)
		except :
			stop()
			return
		if len(list_contact()) < edit :
			print(f"\n    Ce nombre est trop grand, vous n'avez que {len(list_contact())} contacts...")
			time.sleep(wait)
		else :
			break
	liste = list_contact()
	sous_liste = liste[edit - 1]
	print("\n    Vous avez décidé de modifier le contact suivant :")
	time.sleep(wait)
	print(f"\n    {edit} - {sous_liste[0]}   {sous_liste[1]}   {sous_liste[2]}")
	time.sleep(wait)
	print(f"\n    Que voulez vous modifier ?\n")
	time.sleep(wait)
	print("		1 - Prénom")
	time.sleep(wait)
	print("		2 - Nom")
	time.sleep(wait)
	print("		3 - Numéro")
	time.sleep(wait)
	ed = int(input("\n    Entrez ici : "))
	time.sleep(wait)
	if ed == 1 :
		sous_liste[0] = input("\n    Entrez le nouveau Prénom ici : ")
		time.sleep(wait)
	if ed == 2 :
		sous_liste[1] = input("\n    Entrez le nouveau Nom ici : ")
		time.sleep(wait)
	if ed == 3 :
		sous_liste[2] = input("\n    Entrez le nouveau Numéro ici : ")
		time.sleep(wait)
	liste[edit - 1] = sous_liste
	new, count = "", 0
	for i in liste :
		for j in i :
			count += 1
			new += j
			if count != 3 :
				new += " "
		count = 0
		new += "\n"
	with open("repertoire.csv", "w") as f :
		f.write(new)
	print(f"\n    Le contact a bien été modifié en :")
	time.sleep(wait)
	print(f"\n    {edit} - {sous_liste[0]}   {sous_liste[1]}   {sous_liste[2]}")
	time.sleep(wait)
	print("\n    Retour au menu principal.")
	return

def delete() :
	print("\n    Savez vous quels contact vous souhaitez supprimer ou souhaitez vous les afficher ?\n")
	time.sleep(wait)
	print("	1 - Afficher les contacts")
	time.sleep(wait)
	print("	2 - Supprimer un contact")
	time.sleep(wait)
	if 1 == int(input("\n    Entrez un nombre ici : ")) :
		time.sleep(wait)
		liste = list_contact()
		print(f"\n    Vous avez {len(liste)} contacts :")
		time.sleep(wait)
		count = 0
		for i in liste :
			count += 1
			print(f"\n    {count} - {i[0]}   {i[1]}   {i[2]}")
			time.sleep(.050)
	del_list = []
	print("\n    Entrez le numéro du (des) contacts que vous souhaitez supprimer, ou \"stop\" pour arreter.\n")
	time.sleep(wait)
	while True :
		elmt = input("    Entrez ici : ")
		time.sleep(wait)
		try :
			elmt = int(elmt)
		except :
			break
		if len(list_contact()) < elmt :
			print("    Ce nombre est trop grand...")
			time.sleep(wait)
		else :
			del_list.append(int(elmt))
	if len(del_list) == 0 :
		print("\n    Vous n'avez choisi aucun contact... ")
		time.sleep(wait)
		print("    Retour au menu principal.")
		return
	del_list = [del_list[i] for i in range(len(del_list)) if del_list[i] not in del_list[:i]]
	print("\n    Etes vous sur de vouloir supprimer les contacts suivants ?")
	time.sleep(wait)
	print("    Cette action est irréversible.\n")
	time.sleep(wait)
	for i in range(len(del_list)) :
		del_list[i] = del_list[i] - 1
	liste = list_contact()
	for i in range(len(del_list)) :
		sous_liste = liste[del_list[i]]
		print(f"""\n    {del_list[i] + 1} - {sous_liste[0]}   {sous_liste[1]}   {sous_liste[2]}""")
		time.sleep(wait)
	if input("\n    Entrez \"stop\" pour arrêter et n'importe quelle touche pour confirmer : ") == "stop" :
		stop()
		return
	with open("repertoire.csv", "r") as f :
		old = f.read()
	count, char, contact, liste = 0, "", "", ""
	for i in old :
		if i == "\n" :
			contact += "".join(char)
			if count not in del_list :
				liste += contact
				liste += "\n"
			count += 1
			char, contact = "", ""
		else :
			char += i
	with open("repertoire.csv", "w") as f :
		f.write(liste)
	print("\n    Les contacts ont bien étés supprimés.")
	time.sleep(wait)
	print("    Retour au menu principal.\n")
	return

while True :
	time.sleep(wait)
	print(f"\n    Vous avez {len(list_contact())} contacts.\n")
	time.sleep(wait)
	print("    Entrez le numéro correspondant à l'action que vous souhaitez effectuer :\n")
	time.sleep(wait)
	print("    1 - Créer un nouveau contact")
	time.sleep(wait)
	print("    2 - Chercher un contact")
	time.sleep(wait)
	print("    3 - Afficher tous les contacts")
	time.sleep(wait)
	print("    4 - Modifier un contact")
	time.sleep(wait)
	print("    5 - Supprimer un contact")
	time.sleep(wait)
	print("    9 - Quitter votre répertoire\n")
	time.sleep(wait)
	action = input("    Entrez ici : ")
	time.sleep(wait)
	try :
		action = int(action)
		if action == 1 :
			add()
		elif action == 2 :
			search()
		elif action == 3 :
			show()
		elif action == 4 :
			edit()
		elif action == 5 :
			delete()
		elif action == 9 :
			break
		elif action == 137 :
			cheh()
		elif action == 666 :
			print("\n    Deaaaaaam Bro !")
		elif action == 28 :
			print(list_contact())
	except :
		if action == "chocolatine" :
			print("\n    Chocolatines will rrrule the world !")

print("""
			  Au revoir !

		M A D E   B Y   L A D N E W G G

	""")